// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'angularMoment'])

.run(function($ionicPlatform, amMoment, $rootScope, $state, $stateParams) {
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;

  $ionicPlatform.on('resume', function () {
    $rootScope.$broadcast('resume');
  });
  
  amMoment.changeLocale('lt');

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($ionicConfigProvider) {
  $ionicConfigProvider.tabs.position("bottom");
  $ionicConfigProvider.views.maxCache(0);
  // $ionicConfigProvider.tabs.style("standard");
  // $ionicConfigProvider.platform.ios.tabs.style('standard');
  // $ionicConfigProvider.platform.ios.tabs.position('bottom');
  // $ionicConfigProvider.platform.android.tabs.style('ios');
  // $ionicConfigProvider.platform.android.tabs.position('ios');

  // $ionicConfigProvider.platform.ios.navBar.alignTitle('center');
  // $ionicConfigProvider.platform.android.navBar.alignTitle('left');

  // $ionicConfigProvider.platform.ios.backButton.previousTitleText('').icon('ion-ios-arrow-thin-left');
  // $ionicConfigProvider.platform.android.backButton.previousTitleText('').icon('ion-android-arrow-back');

  // $ionicConfigProvider.platform.ios.views.transition('ios');
  $ionicConfigProvider.platform.android.views.transition('ios');
})

.config(function ($stateProvider, $urlRouterProvider, $httpProvider, $injector) {
  $httpProvider.interceptors.push('networkErrorInterceptor');

  $stateProvider

  .state('selectGroup', {
    url: "/",
    controller: 'GroupsCtrl',
    cache: false,
    templateUrl: 'templates/groups-select.html'
  })

  .state('consultations', {
    url: "/consultations",
    controller: 'ConsultationsCtrl',
    templateUrl: 'templates/consultations.html'
  })

  .state('timetables', {
    url: "/timetables",
    controller: 'TimetablesCtrl',
    templateUrl: 'templates/timetables.html'
  })

  // .state('ui', {
  //   url: "/medeine",
  //   abstract: true,
  //   cache: false,
  //   templateUrl: "templates/tabs.html",
  // })

  // .state('ui.timetables', {
  //   url: '/timetables',
  //   cache: false,
  //   views: {
  //   'timetables': {
  //     cache: false,
  //     templateUrl: 'templates/tab-dash.html',
  //     controller: 'TimetablesCtrl'
  //   },
  //   'settings': {
  //     cache: false,
  //     templateUrl: 'templates/tab-account.html',
  //     controller: 'SettingsCtrl'
  //   }}
  // })
  ;

  $urlRouterProvider.otherwise(function ($injector) {
    $injector.get('$state').go('timetables');
  });

});
