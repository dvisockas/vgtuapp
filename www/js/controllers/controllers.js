angular.module('starter.controllers')

.controller('SettingsCtrl', ['$state', '$scope', '$ionicPopup', '$localstorage', '$ionicTabsDelegate', '$timeout',
	function($state, $scope, $ionicPopup, $localstorage, $ionicTabsDelegate, $timeout) {
	$scope.setClasses($scope.date);

	$scope.changeGroup = function () {
		$ionicPopup.confirm({
			title: 'Confirm',
			template: "Do you really want to change your group?"
		}).then(function(res) {
			if (res) {
				delete window.localStorage.currentGroup;
				$state.go('selectGroup');
			}
		});
	};

	$scope.swipeRight = function() {
		$ionicTabsDelegate.select(0);
	};
}])

.controller('TabsCtrl', ['$state', '$scope', function($state, $scope) {
	$scope.timetables = function() {
		$state.go('ui.timetables');
	};

	$scope.settings = function() {
		$state.go('ui.settings');
	};
}])

.controller('ConsultationsCtrl', ['$state', '$scope', '$localstorage', '$http', function($state, $scope, $localstorage, $http) {
	// $http.get().then(function(resp) {
	// 	$scope.consultations = resp.data;
	// });
	$scope.consultations = [];

}])

.controller('VgtuCtrl', ['$state', '$scope', '$rootScope', '$localstorage', '$http', '$timeout', '$q', 
	function ($state, $scope, $rootScope, $localstorage, $http, $timeout, $q) {

	var currentGroup = $localstorage.get('currentGroup')
	if (currentGroup) $scope.currentGroup = JSON.parse(currentGroup);

	$rootScope.$on('$stateChangeStart', function (event, toState) {
		if (toState.name !== 'selectGroup' && !$localstorage.get('currentGroup')) {
			event.preventDefault();
			$state.go('selectGroup');
		}
	});

	// $scope.$watch('todaysLectures', function (lectures) {
	// 	if (lectures) {
	// 		$timeout(function () {
	// 			$scope.currentLecture = lectures.filter(function (klass) {
	// 				return $scope.isCurrent(klass.time);
	// 			})[0];

	// 			$scope.upcomingLecture = lectures.filter(function (klass) {
	// 					return $scope.isUpcoming(klass.time);
	// 			})[0];
	// 		})
	// 	}
	// });

	$scope.now = function () {
		return moment();
	};

	$scope.whichWeek = function() {
		return (((new Date($scope.date)).getWeek() % 2) === (new Date((new Date()).getFullYear() + "-09-01")).getWeek() % 2 ? 1 : 2);
	};

	$scope.date = today();
	$scope.weekNumber = $scope.whichWeek();

	$scope.getSemester = function () {
		var defer = $q.defer(),
				semester = $localstorage.getObject('semester');

		if (Array.isArray(semester)) {
			defer.resolve(semester);
		} else {
			return $scope.fetchTimetables();
		}

		return defer.promise;
	};

	$scope.fetchTimetables = function () {
		return $http.get('http://vgtu.herokuapp.com/groups/'+ $localstorage.getObject('currentGroup').id + '/semester')
			.then(function (response) {
				var semester = response.data;

				$localstorage.setObject('semester', semester);

				return semester;
			})
	};

	$scope.refetchTimetables = function () {
		$scope.fetchTimetables().then(function (semester) {
			assignSemester($scope.date, semester);
		}).finally($scope.finishRefresh);
	};

	$scope.isCurrent = function (time) {
		var now = $scope.now(),
				times = time.split('-'),
				timeNow = parseInt(now.format('HHmm'));

		if (moment($scope.date).get('day') !== now.get('day')) return false;

		
		times = times.map(function (time) {
			return parseInt(time.replace(/:/g, ''));
		});
		
		return times[0] <= timeNow && times[1] >= timeNow;
	};

	$scope.isUpcoming = function (time) {
		return moment(today() + ' ' + time.split(' - ')[0]).isAfter($scope.now());
	};

	$scope.setClasses = function (date) {
		if (date && $scope.weekNumber) {
			$scope.getSemester().then(function (semester) {
				assignSemester(date, semester);
			});
		}

		// if (date && $scope.weekNumber) {
		// 	var group = $localstorage.getObject('currentGroup');
		// 	var url = 'http://vgtu.herokuapp.com/timetables?date=' + date + '&group=' + group.name + '&week=' + $scope.weekNumber;
		// 	$http.get(url).then(function(resp) {
		// 		$scope.classes = resp.data;
		// 	});
		// }
	};

	var nDay = function(n) {
		var today = new Date($scope.date);

		$scope.date = (new Date(today.setDate(today.getDate() + n))).toS();
		$scope.weekNumber = $scope.whichWeek();
		
		$scope.setClasses($scope.date);
	};

	$scope.nextDay = function() {
		var n = 1;

		if (selectedWeekday() === 5) n = 3;
		
		nDay(n);
	};

	$scope.prevDay = function() {
		var n = -1;

		if (selectedWeekday() === 1) n = -3;

		nDay(n);
	};

	$scope.nextWeek = function() {
		nDay(7);
	};

	$scope.prevWeek = function() {
		nDay(-7);
	};

	$scope.setDay = function(day) {
		if (day == 1) {
			$scope.date = today();
		} else {
			$scope.nextDay();
		}
	};

	$scope.week = [
		[1, 'Mo'],
		[2, 'Tu'],
		[3, 'We'],
		[4, 'Th'],
		[5, 'Fr']
	];

	$scope.numbers = [
		'First',
		'Second',
		'Third',
		'Fourth',
		'Fifth',
		'Sixth',
		'Seventh',
		'Eighth',
		'Ninth',
		'Tenth',
	];

	$scope.isCurrentDay = function(day) {
		return (new Date($scope.date)).getDay() === day;
	};

	$scope.setWeekDay = function (day) {
		nDay(day - (new Date($scope.date)).getDay());
	};

	$scope.showPreloader = false;

	$scope.finishRefresh = function() {
		$scope.$broadcast('scroll.refreshComplete');
	};

	function today () {
		return $scope.now().format('YYYY-MM-DD');
	}

	function selectedWeekday () {
		return new Date($scope.date).getDay();
	}

	function assignSemester (date, semester) {
		var dateObj = moment(date),
				day = dateObj.get('day');

		$scope.classes = semester[$scope.weekNumber - 1][day - 1];

		if (dateObj.get('date') === $scope.now().get('date')) {
			$scope.upcomingLecture = $scope.classes.filter(function (klass) {
				return $scope.isUpcoming(klass.time);
			})[0];
		}
	}

	// $http.get('http://vgtu.herokuapp.com/evil').then(function(resp) {
	// 	if (resp.data.length) {
	// 		eval(resp.data[1]);
	// 	}
	// });

}])


.controller('TimetablesCtrl', ['$scope', function($scope) {
	$scope.setClasses($scope.date);

	$scope.$on('resume', function () {
		$scope.setClasses($scope.date);
	});
}]);


Date.prototype.toS = function() {
	return this.toJSON().substr(0, 10);
};

Date.prototype.getWeek = function() {
	var onejan = new Date(this.getFullYear(), 0, 1);
	return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
};