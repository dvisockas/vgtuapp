angular.module('starter').directive('subheader', [function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          var iid = setInterval(function() {
            if (element[0].parentNode) {
              element[0].parentNode.parentNode.insertBefore(element[0], element[0].parentNode);
              clearInterval(iid);
            }
          }, 50);
        }
    };
}]);
