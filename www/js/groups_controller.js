angular.module('starter.controllers', [])

.controller('GroupsCtrl', ['$scope', '$state', '$http', '$timeout', '$localstorage', function ($scope, $state, $http, $timeout, $localstorage) {
  $scope.data = {
    search: '',
    groups: []
  };

  $scope.getGroups = function() {
    $http.get('http://vgtu.herokuapp.com/groups/search?q=' + $scope.data.search)
      .then(function(resp) {
        $scope.data.groups = resp.data;
      });
  };

  $scope.clearInput = function() {
    $scope.data.search = '';
  };

  $scope.setGroup = function(name, id) {
    var group = {
      name: name,
      id: id
    };
    $localstorage.setObject('currentGroup', group);

    $http.get('http://vgtu.herokuapp.com/groups/' + group.id + '/semester').then(function(resp) {
      $localstorage.setObject('semester', resp.data);
      
      $state.transitionTo('timetables', {}, {
        reload: true,
        inherit: false,
        notify: true
      });
    });
  };

}]);
