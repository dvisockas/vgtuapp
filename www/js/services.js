angular.module('starter.services', [])

.factory('Groups', function() {

  return {
    all: function() {
      return chats;
    }
  }
})

.factory('$localstorage', ['$window', function ($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '[]');
    },
    remove: function (key) {
      delete $window.localStorage[key];
    }
  }
}]).factory('networkErrorInterceptor', ['$q', function ($q) {
  return {
    responseError: function (error) {
      if (!error.data && error.status === 0 && !error.statusText.length) {
        alert('Please connect to the internet and try again. Varguoli');
      }
      return $q.reject(error);
    }
  }
}]);
